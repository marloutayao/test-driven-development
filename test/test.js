// chai-http -> tp test HTTP Request
const http = require("chai-http");
const assert = require("assert");
const chai = require("chai");

chai.use(http);

// use the expect assertion style from the chai module
const expect = chai.expect;

// strart out App
const app = require("../App");

// make sure our test framework Mocha is working as expected
// describe() -> is used to group unit tests that are related to each under one test suite
xdescribe("Array", () => {
	describe("indexOf()", () => {
		// it() -> is a single unit test
		it("should return -1 when the given value is not in the array", () => {
			// in a unit test, assertions will be written to tell Mocha what should happen in order for the test to pass
			assert.equal([1, 2, 3, 4].indexOf(5), -1);
		});
	});
});

// test the existence of our application
describe("App", () => {
	it("should exist", () => {
		expect(app).to.be.a("function");
	});

	// async -> will return a promise object
	it("GET / should return status code 200 and a message", async () => {
		// use the request() method of chai-http so send a GET request to the '/'
		return await chai
			.request(app)
			.get("/")
			.then(res => {
				// write assertions for the response returned by our async function
				expect(res).to.have.status(200);
				expect(res.body.message).to.contain("It works!");
			});
	});

	// describe a test suite with unit tests covering user registration
	describe("user registration", () => {
		it("should return status code 201 and a confirmation for valid inputs", async () => {
			// mock a valid user inputs.
			const new_user = {
				name: "Lao Wait",
				email: "lao@wait.com",
				password: "laowait"
			};

			// use chai-http to send a POST request to /register
			// send() attaches the mock input to the request
			return await chai
				.request(app)
				.post("/register")
				.type("form")
				.send(new_user)
				.then(res => {
					expect(res).to.have.status(201);
					expect(res.body.message).to.be.equal("Account registered!");
					expect(res.body.errors.length).to.be.equal(0);
				});
		});
	});

	describe("user login", () => {
		it("should return a tatus code 200 and a JWT", async () => {
			// mock user input
			const valid_input = {
				email: "lao@wait.com",
				password: "laowait"
			};
			return await chai
				.request(app)
				.post("/login")
				.type("form")
				.send(valid_input)
				.then(res => {
					expect(res).to.have.status(200);
					expect(res.body.token).to.exist;
					expect(res.body.message).to.be.equal("login success");
					expect(res.body.errors.length).to.be.equal(0);
				});
		});
	});
});
