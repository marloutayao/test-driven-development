// load express js
const express = require("express");

// require body parser
const bodyParser = require("body-parser");

// require jsonwebtoken JWT

const jwt = require("jsonwebtoken");

// init the express app
const app = express();

// middleware to parse requests of type extended urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// middleware to parse requests of type json
app.use(bodyParser.json());

// define routes in our app
app.get("/", (req, res) => {
	return res.status(200).json({
		message: "It works!"
	});
});

app.post("/login", (req, res) => {
	let hasError = false;
	let errors = [];

	if (!req.body.email) {
		errors.push({
			name: "Email was not received"
		});
		hasError = true;
	}

	if (!req.body.password) {
		errors.push({
			password: "Password was not received"
		});
		hasError = true;
	}

	if (hasError) {
		return res.status(422).json({
			message: "Invalid input",
			errors: errors
		});
	} else {
		//  if email and pass is equal
		if (
			req.body.email === "lao@wait.com" &&
			req.body.password === "laowait"
		) {
			let token = jwt.sign({ name: "Lao Wait" }, "sikretongmalupet");
			return res.status(200).json({
				message: "login success",
				errors: errors,
				token: token
			});
		} else {
			return res.status(401).json({
				message: "wrong credentials",
				errors: errors
			});
		}
	}
});

app.post("/register", (req, res) => {
	let hasError = false;
	let errors = [];

	if (!req.body.name) {
		errors.push({
			name: "Name was not received"
		});
		hasError = true;
	}

	if (!req.body.email) {
		errors.push({
			name: "Email was not received"
		});
		hasError = true;
	}

	if (!req.body.password) {
		errors.push({
			name: "Password was not received"
		});
		hasError = true;
	}

	if (hasError) {
		return res.status(422).json({
			message: "Invalid input",
			errors: errors
		});
	} else {
		return res.status(201).json({
			message: "Account registered!",
			errors: errors
		});
	}
});

// export this module

module.exports = app;
